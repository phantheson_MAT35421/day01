<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day02 - form</title>
    <link rel="stylesheet" type="text/css" href="./bootstrap-5.3.2/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="div-content" style=" padding: 2%;">

            <form style="border: 4px solid #34aeeb; border-radius: 5px;">
            <div class="row g-3 align-items-center" style="margin: auto;">
                <div class="col-3"></div>
                <div class="col-6" style="background-color: gray; padding-top: 5px; padding-bottom: 5px; margin-left: -0.5%;">
                    <p class="g-3" style="line-height: auto; padding-top: 1%;">
                        <?php 
                        $date_time = new DateTime();
                        $time_zone = new DateTimeZone('Asia/Ho_Chi_Minh');

                        $date_time->setTimezone($time_zone);

                        $thu = $date_time->format('N');
                        $thu_dang_chu = ["thứ 2","thứ 3","thứ 4","thứ 5","thứ 6","thứ 7","CN"];

                        $ngay = $date_time->format('d');

                        $thang = $date_time->format('m');

                        $nam = $date_time->format('Y');

                        $gio_phut = $date_time->format('H:i');

                        echo "Bây giờ là: ".$gio_phut.", ".$thu_dang_chu[$thu - 1]." ngày ".$ngay."/".$thang."/".$nam;
                            

                        ?>
                    </p>
                </div>
                <div class="col-1"></div>
            </div>

                <div class="form-group row align-items-center" style="padding: 1% 0;">
                    <div class="col-3"></div>
                        <label for="staticEmail" class="col-sm-2 col-form-label" style="background-color: #3495eb; border: 3px solid #3477eb ;">Tên đăng nhập</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="staticEmail" placeholder="Nhập Tên đăng nhập" style="border-radius: 0px; border: 3px solid #3477eb">
                    </div>
                </div>
                <div class="form-group row align-items-center" style="padding: 1% 0;">
                    <div class="col-3"></div>
                        <label for="inputPassword" class="col-sm-2 col-form-label" style="background-color: #3495eb; border: 3px solid #3477eb ;">Mật khẩu</label>
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="inputPassword" placeholder="Nhập mật khẩu" style="border-radius: 0px; border: 3px solid #3477eb">
                    </div>
                </div>

                <div class="form-group row align-items-center" style="padding: 1% 0;">
                    <div class="col" style="display: flex;">
                        <button type="button" class="btn btn-primary" style="margin: auto; padding: 0.5% 5%;">Đăng nhập</button>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
    
</body>
<script type="text/javascript" src="./bootstrap-5.3.2/js/bootstrap.min.js"></script>
</html>
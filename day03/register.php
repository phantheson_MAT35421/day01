<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day04 - form</title>
    <!-- <link rel="stylesheet" type="text/css" href="../day02/bootstrap-5.3.2/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="./style.css">
</head>
<?php
    $gender = [
        1 => "Nam",
        0 => "Nữ"
    ];
    $major = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    ];
?>
<body>
    <div class="container">
        <div class="form-border">
            <form action="" id="form-login">
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên</label>
                    <input type="text" id="name" class="form-control" placeholder="Nhập tên vào đây" required>
                </div>

                <div class="form-group">
                    <label for="" class="form-label">Giới tính</label>
                    <?php
                        for ($i = 0; $i < count($gender); $i++) {
                            ?>
                                <input type="radio" id="gender-<?php echo $i; ?>" value="<?php echo $i; ?>" class="form-control" name = "gender">
                                <label for="<?php echo "gender-".$i; ?>"><?php echo $gender[$i]; ?></label>
                            <?php
                        }
                    ?>
                    
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Phân khoa</label>
                    <select name="" id="marjor" class="form-control">
                        <option value="0" selected>--Chọn phân khoa--</option>
                    <?php
                        foreach($major as $key => $value){
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                        }
                    ?>
                    </select>
                </div>  
                <div class="form-group">
                    <input type="submit" class="btn-submit form-control" value="Đăng ký">
                </div>         
            </form>
        </div>
    </div>
</body>

<!-- <script type="text/javascript" src="../day02/bootstrap-5.3.2/js/bootstrap.min.js"></script> -->
</html>
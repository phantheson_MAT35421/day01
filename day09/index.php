
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <link rel="stylesheet" type="text/css" href="./bootstrap-5.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
</head>
<?php
include "./config.php";
include "./process_image.php";
$db = new database();

if (isset($_POST['btn_submit'])) {
    $username = $_POST['username'];
    $gender = $_POST['gender'];
    $department = $_POST["department"];
    $date = $_POST["date"];
    $address = $_POST['address'];
    $img_name = process_image();

    
    $sql = "INSERT INTO students (ID, HoTen, Khoa, GioiTinh, NgaySinh, DiaChi,Anh) VALUES (NULL,?,?,?,?,?,?)";
    $db->setQuery($sql);
    $result = $db->execute([$username,  $department, $gender, $date, $address, $img_name]);

    if ($result){
        ?>
            <script>alert("Thêm dữ liệu thành công")</script>
        <?php
        unset($_POST['btn_submit']);
    }
    else {
        ?>
            <script>alert("Thêm dữ liệu thất bại")</script>
        <?php
        header("location: register.php");
    }
}

$gender = [
        
    0 => "Nữ",
    1 => "Nam"
];
$major = [
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
];

$sql = "SELECT * FROM students";
$db->setQuery($sql);
$rs = $db->loadAllRows();
// var_dump($rs);
// die();

?>
<body>
    <div style="background-color: gray; z-index: -99;">
        <div class="container" style="padding: 2% 10%; background-color: white;">
            <div class="form_area" style="padding: 5% 15%; border-radius: 10px;">
                <form id="form_search">
                    <div class="form-group">
                        <label for="khoa">Khoa</label>
                        <select name="department" id="marjor" class="form-control">
                        <option value="0" selected></option>
                    <?php
                        foreach($major as $key => $value){
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                        }
                    ?>
                    </select>
      
                    </div>
                    <div class="form-group">
                        <label for="tu_khoa">Từ Khoá</label>
                        <input type="text" class="form-control" id="tu_khoa">
                    </div>
                    <div class="form-group" style="display: flex; padding-top: 15px;">
                        <input type="submit" class="btn btn-primary" name="btn_submit" id="search_btn" value="Reset" style="margin: auto;">
                    </div>
                </form>
            </div>

            <div class="table_area">
                <div style="display: flex; justify-content: space-between; align-items: center;">
                    <div class="number_student">
                        <p>Số sinh viên tìm thấy: <?php echo count($rs); ?></p>
                    </div>
                    <div class="add_student">
                        <a href="./register.php"><button type="button" class="btn btn-primary">Thêm Sinh viên</button></a>
                    </div>
                </div>
                <table class="table table-striped" id="">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Họ Tên</th>
                        <th scope="col">Khoa</th>
                        <th scope="col">Giới tính</th>
                        <th scope="col">Ngày sinh</th>
                        <th scope="col">Địa chỉ</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody id="tbody_table">
                        <?php
                            $count = 1;
                            foreach ($rs as $key => $value) {
                        ?>
                        <tr id="std_<?php echo $value->ID; ?>">
                            <th scope="row"><?php echo $count; ?></th>
                            <td><?php echo $value->HoTen; ?></td>
                            <td><?php echo $major[$value->Khoa]; ?></td>
                            <td><?php echo $gender[$value->GioiTinh]; ?></td>
                            <td><?php echo $value->NgaySinh; ?></td>
                            <td><?php echo $value->DiaChi; ?></td>
                            <td><img src="image/<?php echo $value->Anh; ?>" style="max-width: 50px; max-height: 50px;"> </td>
                            <td>
                                <a href="./modify_record.php?id=<?php echo $value->ID; ?>&action=modify"><button type="button" class="btn btn-primary"><i class="bi bi-wrench"></i></button></a>
                                <button type="button" class="btn btn-warning delete"><i class="bi bi-trash-fill"></i></button>
                                <input type="hidden" id="student_ID" value="<?php echo $value->ID; ?>">
                            </td>
                        </tr>
                        <?php
                        $count++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="./jquery-3.7.1.min.js"></script>
<script type="text/javascript" src="./bootstrap-5.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./bootstrap-5.3.2/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="./bootstrap-5.3.2/js/"></script>
<script>

    $(document).ready(function(){
        $(".delete").click(function(){
            var accept = confirm("are you sure delete this record!")
            var element = $(this).closest("tr")
            if (accept){
                var id = $("#student_ID").val()
                var action = "delete"
                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: {status: action, id: id},
                    dataType: "JSON",
                    success:function(response){
                        
                        if (response.status === "success!"){
                            alert("Delete successfully! status:" + response.status)
                            element.remove()

                            updateNumber()
                        }
                    },
                    error: function () {
                        alert("An error occurred while processing your request. status:" + response.status );
                    }
                })
                
            } else{
                alert("Có Lỗi xảy ra")
            }
        })
        function updateNumber(){
            $("#tbody_table tr").each(function (index) {
                $(this).find("th:first").text(index + 1);
            });
        }
        $('#form_search').on('submit', function(e){
            e.preventDefault();
            $('#tu_khoa').val(''); 
            $('#marjor').val('0'); 
            performSearch()
        })

        $('#marjor').change(function(){
            performSearch()
        })

        $('#tu_khoa').keyup(function(){
            performSearch()
        })

        function performSearch() {
            var action = "search";
            var department = $("#marjor").val();
            var name = $('#tu_khoa').val();
            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: { name: name, department: department, action: action },
                dataType: "JSON",
                success: function(response) {
                    if (response.status === "success") {
                        $('#tbody_table').html(response.row);
                        $('.number_student').html('<p>Số sinh viên tìm thấy: ' + response.num + '</p');
                    }
                }
            });
        }
    })
</script>
</html>
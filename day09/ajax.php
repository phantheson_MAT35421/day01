<?php

$gender = [
        
    0 => "Nữ",
    1 => "Nam"
];
$major = [
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
];

if (!empty($_POST["action"])) {
    include "./config.php";
    $db = new database();

    
    $department = $_POST['department'];
    $name = $_POST['name'];
    if ($department == '0'){
        $department = '';
    }
    $sql = "SELECT * FROM students WHERE students.Khoa LIKE '%".$department."%' AND students.HoTen LIKE '%".$name."%'";
    $db->setQuery($sql);

    $re = $db->loadAllRows(array($department, $name));
    $num_rs = count( $re );
    if (isset($re)) {
        $count = 1;
        $row = '';
        foreach ($re as $key => $value) {
            $row .= ' <tr>
                <th scope="row">'.$count.'</th>
                <td>'.$value->HoTen.'</td>
                <td>'.$major[$value->Khoa].'</td>
                <td>'.$gender[$value->GioiTinh].'</td>
                <td>'.$value->NgaySinh.'</td>
                <td>'.$value->DiaChi.'</td>
                <td><img src="image/'.$value->Anh.'" style="max-width: 50px; max-height: 50px;"> </td>
                <td>
                    <a href="./modify_record.php?id='.$value->ID.'"><button type="button" class="btn btn-primary"><i class="bi bi-wrench"></i></button></a>
                    <a><button type="button" class="btn btn-warning"><i class="bi bi-trash-fill"></i></button></a>
                </td>
            </tr>';
            $count++;
        }
    }
    echo json_encode(array("status"=> "success","message"=> "successfully!", "row" => $row, "num" => $num_rs));
}
if ($_POST["status"]=="delete") {
    include("remote.php");
    $db2 = new remote();
    echo $db2->delete_record_by_id($_POST['id']);
}
?>
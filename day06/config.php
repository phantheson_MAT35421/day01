<?php 

class config{
    public $servername;
    public $username;
    public $password;
    public $dbname;
    public function __construct(){
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "";        
        $this->dbname = "ltweb";    
    }

    public function getBD(){
        try {
            $conn = new PDO("mysql:host=$this->servername;dbname=ltweb", $this->username, $this->password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Connected successfully";
            return $conn;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
        return null;
    }

}

?>
<?php
    function process_image(){
        if (isset($_FILES['image'])){
            if ($_FILES['image']['error'] == 0){
                $target_dir    = "image/";

                $target_file   = $target_dir . basename($_FILES["image"]["name"]);

                $allowUpload   = true;

  
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION); #format JPG

  
                $maxfilesize   = 1600000;

  
                $allowtypes    = array('jpg', 'png', 'jpeg', 'gif');

                $check = getimagesize($_FILES["image"]["tmp_name"]);

                if($check !== false)
                {
                    $allowUpload = true;
                }
                else
                {
                    $allowUpload = false;
                }

                if (file_exists($target_file))
                {
                    echo "Tên file đã tồn tại trên server, không được ghi đè";
                    $allowUpload = false;
                }
                if ($_FILES["image"]["size"] > $maxfilesize)
                {
                    echo "Không được upload ảnh lớn hơn $maxfilesize (bytes).";
                    $allowUpload = false;
                }

                if (!in_array($imageFileType,$allowtypes ))
                {
                    echo "Chỉ được upload các định dạng JPG, PNG, JPEG, GIF";
                    $allowUpload = false;
                }


                if ($allowUpload)
                {
                    
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
                    {
                        
                        echo '<img src="./image/'.$_FILES["image"]['name'].'" alt="" style="width: 100px; height: 100px; max-width: 100%; justify-content: left;">';

                    }
                    else
                    {
                        echo "Có lỗi xảy ra khi upload file.";
                    }
                }
            } else {
                echo "File Ảnh bị lỗi !";
                die;
            }
        } else {
            echo "Lỗi upload file!";
            die;
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day05 - form</title>
    <!-- <link rel="stylesheet" type="text/css" href="../day02/bootstrap-5.3.2/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="./my_style.css">
</head>
<?php
    include "./process_image.php";

    $gender = [
        
        0 => "Nữ",
        1 => "Nam"
    ];
    $major = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    ];
    // echo "<pre>";
    // echo print_r($_FILES["image"]);
?>
<body>
    <div class="container">
        <div class="form-border">
            
            <form method="" id="form-login">
                <div id="alert_form"></div>
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên</label>
                    <div class="form_after_submit"><?php echo $_POST['username']; ?></div>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Giới tính</label>
                    <div class="form_after_submit"><?php echo $gender[$_POST['gender']]; ?></div>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Phân khoa</label>
                    <div class="form_after_submit"><?php echo $major[$_POST['department']]; ?></div>
                </div>  
                <div class="form-group">
                    <label for="date_time" class="form-label">Ngày sinh</label>
                    <div class="form_after_submit"><?php 
                    $data_date = explode("-", $_POST['date']); 
                    echo $data_date[2].'/'.$data_date[1].'/'.$data_date[0];
                    ?></div>
                </div>
                <div class="form-group" style="align-items: flex-start;">
                    <label for="address" class="form-label">Địa chỉ</label>
                    <div class="form_after_submit"><?php echo $_POST['address']; ?></div>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Hình ảnh</label>
                    <div class="form_after_submit">
                        
                        <?php process_image(); ?>

                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" name="btn_submit" class="btn-submit form-control" id="submit_btn" value="Xác Nhận">
                </div>         
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="./jquery-3.7.1.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
        $('#form-login').submit(function(e){
            e.preventDefault();
        })
   })
</script>
<!-- <script type="text/javascript" src="../day02/bootstrap-5.3.2/js/bootstrap.min.js"></script> -->
</html>
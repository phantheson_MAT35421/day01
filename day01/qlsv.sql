CREATE DATABASE IF NOT EXISTS QLSV;



CREATE TABLE DMKHOA (
  MaKH varchar(6) NOT NULL PRIMARY KEY,
  TenKhoa varchar(30) NOT NULL
);

CREATE TABLE SINHVIEN (
  MaSV varchar(6) NOT NULL PRIMARY KEY,
  HoSV varchar(30) NOT NULL,
  TenSV varchar(15) NOT NULL,
  GioiTinh char(1) NOT NULL,
  NgaySinh datetime NOT NULL,
  NoiSinh varchar(50) NOT NULL,
  DiaChi varchar(50) NOT NULL,
  MaKH varchar(6) NOT NULL,
  HocBong int(11)
);
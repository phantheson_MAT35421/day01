
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <link rel="stylesheet" type="text/css" href="./bootstrap-5.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
</head>
<?php
include "./config.php";
include "./process_image.php";
$db = new database();

if (isset($_POST['btn_submit'])) {
    $username = $_POST['username'];
    $gender = $_POST['gender'];
    $department = $_POST["department"];
    $date = $_POST["date"];
    $address = $_POST['address'];
    $img_name = process_image();

    
    $sql = "INSERT INTO students (ID, HoTen, Khoa, GioiTinh, NgaySinh, DiaChi,Anh) VALUES (NULL,?,?,?,?,?,?)";
    $db->setQuery($sql);
    $result = $db->execute([$username,  $department, $gender, $date, $address, $img_name]);

    if ($result){
        ?>
            <script>alert("Thêm dữ liệu thành công")</script>
        <?php
        unset($_POST['btn_submit']);
    }
    else {
        ?>
            <script>alert("Thêm dữ liệu thất bại")</script>
        <?php
        header("location: register.php");
    }
}

$gender = [
        
    0 => "Nữ",
    1 => "Nam"
];
$major = [
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
];

$sql = "SELECT * FROM students";
$db->setQuery($sql);
$rs = $db->loadAllRows();
// var_dump($rs);
// die();

?>
<body>
    <div style="background-color: gray; z-index: -99;">
        <div class="container" style="padding: 2% 10%; background-color: white;">
            <div class="form_area" style="padding: 5% 15%; border-radius: 10px;">
                <form>
                    <div class="form-group">
                        <label for="khoa">Khoa</label>
                        <input type="email" class="form-control" id="khoa">
                    </div>
                    <div class="form-group">
                        <label for="tu_khoa">Từ Khoá</label>
                        <input type="text" class="form-control" id="tu_khoa">
                    </div>
                    <div class="form-group" style="display: flex; padding-top: 15px;">
                        <input type="submit" class="btn btn-primary" name="btn_submit" value="Tìm Kiếm" style="margin: auto;">
                    </div>
                </form>
            </div>

            <div class="table_area">
                <div style="display: flex; justify-content: space-between; align-items: center;">
                    <div class="number_student">
                        <p>Số sinh viên tìm thấy: <?php echo count($rs); ?></p>
                    </div>
                    <div class="add_student">
                        <a href="./register.php"><button type="button" class="btn btn-primary">Thêm Sinh viên</button></a>
                    </div>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Họ Tên</th>
                        <th scope="col">Khoa</th>
                        <th scope="col">Giới tính</th>
                        <th scope="col">Ngày sinh</th>
                        <th scope="col">Địa chỉ</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $count = 1;
                            foreach ($rs as $key => $value) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $count; ?></th>
                            <td><?php echo $value->HoTen; ?></td>
                            <td><?php echo $major[$value->Khoa]; ?></td>
                            <td><?php echo $gender[$value->GioiTinh]; ?></td>
                            <td><?php echo $value->NgaySinh; ?></td>
                            <td><?php echo $value->DiaChi; ?></td>
                            <td><img src="image/<?php echo $value->Anh; ?>" style="max-width: 50px; max-height: 50px;"> </td>
                            <td>
                                <a href="./modify_record.php?id=<?php echo $value->ID; ?>"><button type="button" class="btn btn-primary"><i class="bi bi-wrench"></i></button></a>
                                <a><button type="button" class="btn btn-warning"><i class="bi bi-trash-fill"></i></button></a>
                            </td>
                        </tr>
                        <?php
                        $count++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="./jquery-3.7.1.min.js"></script>
<script type="text/javascript" src="./bootstrap-5.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./bootstrap-5.3.2/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="./bootstrap-5.3.2/js/"></script>
</html>
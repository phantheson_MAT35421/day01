<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <!-- <link rel="stylesheet" type="text/css" href="../day02/bootstrap-5.3.2/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="./my_style.css">
</head>
<?php
    include_once("remote.php");
    $obj = new remote();
    $rs = $obj->read_user_by_id($_GET['id']);
    if (isset($_POST['btn_modify'])){

        $username = $_POST['username'];
        $gender = $_POST['gender'];
        $department = $_POST['department'];
        $date = $_POST['date'];
        $address = $_POST['address'];
        $anh = $_POST['temp_img_name'];
        $obj->modify_data($_GET['id'] ,$username, $gender, $department, $date, $address, $anh);

    }
    
    $gender = [
        
        0 => "Nữ",
        1 => "Nam"
    ];
    $major = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    ];

?>
<body>
    <div class="container">
        <div class="form-border">
            
            <form method="post" id="form-login" enctype="multipart/form-data">
                <div id="alert_form"></div>
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên *</label>
                    <input type="text" name="username" id="name" class="form-control" placeholder="Nhập tên vào đây" value="<?php echo $rs->HoTen; ?>">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Giới tính *</label>
                    <?php
                        for ($i = 0; $i < count($gender); $i++) {
                            ?>
                                <input type="radio" id="gender-<?php echo $i; ?>" value="<?php echo $i; ?>" class="gender form-control" name = "gender" <?php echo $rs->GioiTinh == $i ? "checked" : ""; ?>>
                                <label for="<?php echo "gender-".$i; ?>"><?php echo $gender[$i]; ?></label>
                            <?php
                        }
                    ?>
                    
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Phân khoa</label>
                    <select name="department" id="marjor" class="form-control">
                        <option value="<?php echo $rs->Khoa; ?>" selected><?php echo $major[$rs->Khoa]; ?></option>
                    <?php
                        foreach($major as $key => $value){
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                        }
                    ?>
                    </select>
                </div>  
                <div class="form-group">
                    <label for="date_time" class="form-label">Ngày sinh *</label>
                    <input type="date" id="date_time" name="date" class="form-control" value="<?php echo $rs->NgaySinh; ?>">
                </div>
                <div class="form-group" style="align-items: flex-start;">
                    <label for="address" class="form-label">Địa chỉ *</label>
                    <textarea class="form-control" id="address" name="address"><?php echo $rs->DiaChi; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Hình ảnh</label>
                    <label for="image"><img src="./image/<?php echo $rs->Anh; ?>" alt="" style="max-width: 120px; cursor: pointer;"></label>
                    <input type="file" id="image" name="image" class="form-control" value="<?php echo $rs->Anh; ?>" >
                    <input type="hidden" name="temp_img_name" value = "<?php echo $rs->Anh; ?>" >
                </div>
                <div class="form-group">
                    <input type="submit" name="btn_modify" class="btn-submit form-control" id="submit_btn" value="Đăng ký">
                </div>         
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="./jquery-3.7.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
       $("#form-login").on("submit", function(e){
            e.preventDefault()

            var username = $("#name").val()
            var gender = $(".gender:checked").val()
            var department = $("#marjor").val()
            var date_time = $("#date_time").val()
            var address = $("#address").val()

            $(".error_message").remove()
            $(".success").remove()
            
            flag =true
            console.log({username : username, gender: gender, department: department, date_time: date_time, address: address})
            
            if (username === ""){
                flag =false
                $("#alert_form").append("<p class='error_message'>Hãy nhập tên.</p>")
            }
            if (gender === undefined){
                flag =false
                $("#alert_form").append("<p class='error_message'>Giới tính không được để trống.</p>")
            }
            if (department === '0'){
                flag =false
                $("#alert_form").append("<p class='error_message'>Hãy chọn phân khoa.</p>")
            }
            if (date_time === ''){
                flag =false
                $("#alert_form").append("<p class='error_message'>Vui lòng nhập ngày sinh.</p>")
            }
            if (address === ''){
                flag =false
                $("#alert_form").append("<p class='error_message'>Địa chỉ không được để trống.</p>")
            }
            
            submit_form(flag)
       }, function submit_form(flag){
        if (flag){
            $(".success").remove()
            $("#alert_form").append("<p class='success'>Register Successfully!.</p>")
            $("#form-login").submit()     
        }
       })


    })
</script>
<!-- <script type="text/javascript" src="../day02/bootstrap-5.3.2/js/bootstrap.min.js"></script> -->
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Input student</title>
    <link rel="stylesheet" href="./bootstrap-5.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="./style.css">
</head>
<?php
$gender = [
    1 => "Nam",
    2 => "Nữ"
];
$date_time = new DateTime();
$time_zone = new DateTimeZone('Asia/Ho_Chi_Minh');

$date_time->setTimezone($time_zone);

$thu = $date_time->format('N');

$nam = $date_time->format('Y');

$thanh_pho = [
    "HN" => "Hà Nội",
    "HCM" => "Tp. Hồ Chí Minh"
];

?>
<body>
    <div class="container">
        
        <div class="form_area">
            <div class="form_title">
                <h3>Form đăng ký sinh viên</h3>
                <div id="alert_form"></div>
            </div>
            <div class="form_content">
            <form id="form-login" method="POST" action="./regist_student.php">
                <div class="form-group row">
                    <label for="username" class="col-sm-3 col-form-label label-form">Họ và tên</label>
                    <div class="col-sm-9 input-form">
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-sm-3 col-form-label label-form">Giới tính</label>
                    <div class="col-sm-9 input-form">
                        
                        <?php
                            foreach ($gender as $key => $value) {
                        ?>
                            <input type="radio" name="gender" value="<?php echo $key; ?>" class="gender form-check-input">
                            <label><?php echo $value; ?></label>
                        <?php
                            }
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-sm-3 col-form-label label-form">Ngày sinh</label>
                    <div class="col-sm-9 input-form">
                        <div class="row">
                        <div class="col-sm-3 display_flex ">
                            <label for="year">Năm</label>
                            <select name="year" id="year" class="form-control">
                                <option value="0">__Chọn năm__</option>
                                <?php
                                    for ($i = 0; $i <= 55; $i++){
                                ?>
                                    <option value="<?php echo $nam - 40 + $i; ?>"><?php echo $nam - 40 + $i; ?></option>
                                    <?php } ?>
                            </select>

                        </div>
                        <div class="col-sm-3 display_flex">
                            <label for="month">Tháng</label>
                            <select name="month" id="month" class="form-control">
                                <option value="0">__Chọn tháng__</option>
                                <?php
                                    for ($i = 1; $i <= 12; $i++){
                                ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-3 display_flex">
                            <label for="day">Ngày</label>
                            <select name="day" id="day" class="form-control">
                                <option value="0">__Chọn ngày__</option>
                                <?php
                                    for ($i = 1; $i <= 31; $i++){
                                ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                            </select>   
                        </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-sm-3 col-form-label label-form">Địa chỉ</label>
                    <div class="col-sm-9 input-form">
                        
                        <div class="row">
                            <div class="col-sm-6 display_flex">
                                <label for="thanh_pho">Thành Phố</label>
                                <select name="thanh_pho" id="thanh_pho" class="form-control ">
                                    <option value="0">__Thành Phố__</option>
                                    <?php
                                        foreach ($thanh_pho as $key => $value){
                                    ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>   
                            </div>
                            <div class="col-sm-6 display_flex">
                                <label for="quan">Quận</label>
                                <select name="quan" id="quan" class="form-control ">
                                    <option value="0">__Quận__</option>
                                 
                                    
                                    
                                </select>   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="detail" class="col-sm-3 col-form-label label-form">thông tin khác</label>
                    <div class="col-sm-9 input-form">
                        <textarea name="detail" id="detail" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="detail" class="col-sm-3 col-form-label label-form"></label>
                    <div class="col-sm-9 input-form" style="display: flex;">
                        <button type="submit" class="btn btn-primary" id="register" style="margin: auto;">Đăng ký</button>
                    </div>
                    
                </div>
            </form>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="./jquery-3.7.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $("#thanh_pho").change(function(){
            var value = $("#thanh_pho").val()
            if (value == "HN"){
                $("#quan").html('<div id="HN" style="display: none;"><option value="HM">Hoàng Mai</option><option value="TT">Thanh Trì</option><option value="NTL">Nam Từ Liêm</option><option value="HD">Hà Đông</option><option value="CG">Cầu Giấy</option></div>')
            } else if (value == "HCM") {
                $("#quan").html('<div id="HCM"><option value="Q1">Quận 1</option><option value="Q2">Quận 2</option><option value="Q3">Quận 3</option><option value="Q7">Quận 7</option><option value="Q9">Quận 9</option></div>')
            }
        })

        $("#register").on("click", function(e){
            e.preventDefault()

            var username = $("#usernmae").val()
            var gender = $(".gender:checked").val()
            var year = $("#year").val()
            var month = $("#month").val()
            var day = $("#day").val()
            var thanhpho = $("#thanh_pho").val()
            var quan = $("#quan_tp").val()
            var detail = $("#detail").val()

            console.log({username: username, gender: gender, year: year, month: month, day: day, thanhpho: thanhpho, quan: quan, detail: detail})
            $(".error_message").remove()
            $(".success").remove()
            flag =true

            if (username === ""){
                flag =false
                $("#alert_form").append("<p class='error_message'>Hãy nhập tên.</p>")
            }
            if (gender === undefined){
                flag =false
                $("#alert_form").append("<p class='error_message'>Giới tính không được để trống.</p>")
            }
            if (year === '0'){
                flag =false
                $("#alert_form").append("<p class='error_message'>Hãy nhập năm.</p>")
            }
            if (month === '0'){
                flag =false
                $("#alert_form").append("<p class='error_message'>Hãy nhập tháng.</p>")
            }
            if (day === '0'){
                flag =false
                $("#alert_form").append("<p class='error_message'>Hãy nhập ngày.</p>")
            }
            if (thanhpho === '0'){
                flag =false
                $("#alert_form").append("<p class='error_message'>Hãy nhập thành phố.</p>")
                
            }

            if (quan === '0'){
                $("#alert_form").append("<p class='error_message'>Hãy nhập quận.</p>")
            }
            if (flag){
                $("#form-login").submit()
            }
        })

    })
</script>
<script type="text/javascript" src="./bootstrap-5.3.2/js/bootstrap.min.js"></script>
</html>
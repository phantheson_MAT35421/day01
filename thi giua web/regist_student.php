<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Input student</title>
    <link rel="stylesheet" href="./bootstrap-5.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="./style.css">
</head>
<?php
$gender = [
    1 => "Nam",
    2 => "Nữ"
];
$date_time = new DateTime();
$time_zone = new DateTimeZone('Asia/Ho_Chi_Minh');

$date_time->setTimezone($time_zone);

$thu = $date_time->format('N');

$nam = $date_time->format('Y');

$thanh_pho = [
    "HN" => "Hà Nội",
    "HCM" => "Tp. Hồ Chí Minh"
];

?>
<body>
    <div class="container">
        
        <div class="form_area">
            <div class="form_title">
                <h3>Form đăng ký sinh viên</h3>
            </div>
            <div class="form_content">
            <form id="form-login" method="POST" action="./regist_student.php">
                <div class="form-group row">
                    <label for="username" class="col-sm-3 col-form-label label-form">Họ và tên</label>
                    <div class="col-sm-9 input-form">
                        <div><?php echo $_POST['username']; ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-sm-3 col-form-label label-form">Giới tính</label>
                    <div class="col-sm-9 input-form">
                       <div><?php echo $gender[$_POST['gender']]; ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-sm-3 col-form-label label-form">Ngày sinh</label>
                    <div class="col-sm-9 input-form">
                       
                        <div><?php echo $_POST['day'].'/'.$_POST['month'].'/'.$_POST['year']; ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-sm-3 col-form-label label-form">Địa chỉ</label>
                    <div class="col-sm-9 input-form">
                        <div><?php echo ''.$_POST['quan'].' - '.$thanh_pho[$_POST['thanh_pho']].''; ?></div>
                       
                        
                    </div>
                </div>
                <div class="form-group row">
                    <label for="detail" class="col-sm-3 col-form-label label-form">thông tin khác</label>
                    <div class="col-sm-9 input-form">
                        <div><?php echo $_POST['detail']; ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="detail" class="col-sm-3 col-form-label label-form"></label>
                    <div class="col-sm-9 input-form" style="display: flex;">
                        <button type="submit" class="btn btn-primary" id="register" style="margin: auto;">Đăng ký</button>
                    </div>
                    
                </div>
            </form>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="./jquery-3.7.1.min.js"></script>
<script type="text/javascript" src="./bootstrap-5.3.2/js/bootstrap.min.js"></script>
</html>